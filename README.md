# Swingy

Swingy is a game made using Java's Swing Library, it was a school project

The project is compiled to a jar excutable using Maven and he game itself is written to follow the Model-View-Controller architectural pattern

# To run
Make sure to have Maven install and configured correctly on your machine
<br>mvn install
<br>Then open the jar file, Swingy.jar


# What the game looks like

![](https://gitlab.com/Heagan/Swingy/raw/master/Home.PNG)
![](https://gitlab.com/Heagan/Swingy/raw/master/Game.PNG)
![](https://gitlab.com/Heagan/Swingy/raw/master/Battle.PNG)

## Because it was written using the MVC Pattern it was super easy to get it working on the console as well as switch to the gui duing runtime even
![](https://gitlab.com/Heagan/Swingy/raw/master/Console.PNG)



