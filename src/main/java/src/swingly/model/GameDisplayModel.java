package src.swingly.model;

import src.swingly.controller.GameDisplayController;
import src.swingly.controller.MapDisplayController;
import src.swingly.view.GameDisplayView;
import src.swingly.view.MapDisplayView;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static src.swingly.Swing.displayModel;
import static src.swingly.Swing.mapModel;

public class GameDisplayModel {
    GameDisplayController gameDisplayController;
    GameDisplayView gameDisplayView;

    MapDisplayModel mapDisplayModel;

    ButtonClickListener buttonClickListener = new ButtonClickListener();
    ButtonPressedListener buttonPressedListener = new ButtonPressedListener();

    public GameDisplayModel(GameDisplayController gameDisplayController, GameDisplayView gameDisplayView) {
        this.gameDisplayController = gameDisplayController;
        this.gameDisplayView = gameDisplayView;

        this.gameDisplayView.addButtonListeners(buttonClickListener);
        this.gameDisplayView.addKeyListeners(buttonPressedListener);
        this.gameDisplayView.addKeyListener(buttonPressedListener);

        this.mapDisplayModel = new MapDisplayModel(new MapDisplayController(), new MapDisplayView());
        this.mapDisplayModel.mapDisplayView.addKeyListener(buttonPressedListener);

    }

    public void hide() {
        gameDisplayView.setVisible(false);
        mapDisplayModel.hide();
    }

    public void displayFrame(Point location) {
        if (displayModel.getShowGui()) {
            gameDisplayView.setLocation(location);
            gameDisplayView.setVisible(true);
            displayMap(location);

            gameDisplayView.getTfDetails().setText(gameDisplayController.getHeroDetails());
            mapModel.setUpMap();
            refreshMapInfo();
        } else {
            startTextInputLoop();
        }
    }

    public void displayMap(Point location) {
        Point mapLoc = new Point(location.x + gameDisplayView.getWidth(), location.y);

        mapDisplayModel.displayFrame(mapLoc);
    }

    public void refreshMapInfo() {
       String mapInfo = gameDisplayController.refreshMapInfo();

        if (displayModel.showGui) {
            mapDisplayModel.mapDisplayView.setText("");
            mapDisplayModel.mapDisplayView.appendText(mapInfo);
        } else {
            System.out.println(mapInfo);
        }
    }

    public class ButtonClickListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (gameDisplayController.moveEvents(e.getActionCommand())) {
                displayModel.displayBattleFrame(gameDisplayView.getLocation());
            }
            gameDisplayController.moveEnemies();
            refreshMapInfo();
        }
    }

    public class ButtonPressedListener implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (gameDisplayController.moveEvents(getKeyInfo(e))) {
                displayModel.displayBattleFrame(gameDisplayView.getLocation());
            }
            gameDisplayController.moveEnemies();
            refreshMapInfo();
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        private String getKeyInfo(KeyEvent e) {
            int id = e.getID();

            if (id == KeyEvent.KEY_TYPED) {
                char c = e.getKeyChar();

                return (c + "");
            } else {
                int keyCode = e.getKeyCode();

                return (KeyEvent.getKeyText(keyCode));
            }
        }
    }

    private void startTextInputLoop() {
        String input;

        displayModel.displayMessage(
                gameDisplayController.getMessage().get((int)(Math.random() * gameDisplayController.getMessage().size())));
        displayModel.displayQuestion("Ready?");

        mapModel.setUpMap();
        refreshMapInfo();
        displayModel.displayMessage(gameDisplayController.getHeroDetails());
        while (!displayModel.getShowGui()) {
            input = displayModel.displayQuestion("Move: w/a/s/d");
            if (gameDisplayController.moveEvents(getInputDirection(input))) {
                displayModel.displayBattleFrame();
                return ;
            } else {
                mapModel.setUpMap();
                gameDisplayController.moveEnemies();
                refreshMapInfo();
                displayModel.displayMessage(gameDisplayController.getHeroDetails());
            }
        }
    }

    public String getInputDirection(String direction) {
        switch (direction) {
            case("a"): {
                return "Left";
            }
            case("d"): {
                return "Right";
            }
            case("w"): {
                return "Up";
            }
            case("s"): {
                return "Down";
            }
        }
        return "";
    }

}
