package src.swingly.model;

import lombok.Data;
import src.swingly.Actor;
import src.swingly.dependencies.Location;

import java.util.ArrayList;

import static src.swingly.Swing.mapModel;

@Data
public class MapModel {
    private int map[][];
    private int heroIndex;
    private int enemyIndex;
    private ArrayList<Actor> actors = new ArrayList<Actor>();
    private int mapSize = 0;

    public void setMapSize() {
        int level = actors.get(heroIndex).getActorLevel();

        this.mapSize = (level - 1) * 5 + 10 - (level % 2);
    }

    public boolean isLocationInMap(Location location) {
        if (location.getX() <= mapModel.getMapSize() - 1) {
            if (location.getX() >= 0) {
                if (location.getY() <= mapModel.getMapSize() - 1) {
                    if (location.getY() >= 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public Actor getEnemyAtLocation(Location location) {
        for ( int i = 0; i < actors.size(); i++) {
            if (actors.get(i).getLocation().equals(location)) {
                if (!actors.get(i).equals(getHero())) {
                    return (actors.get(i));
                }
            }
        }
        return null;
    }

    public void setEnemyIndexFromActor(Actor actor) {
        ArrayList<Actor> actors = mapModel.getActors();

        for (int i = 0; i < actors.size(); i++) {
            if (actors.get(i).equals(actor)) {
                this.enemyIndex = i;
            }
        }
    }

    public Actor getHero() {
        return (actors.get(heroIndex));
    }

    public boolean  shouldStartBattle(Location location) {
        if (map[location.getY()][location.getX()] == 2) {
            return true;
        }
        return false;
    }

    public void addHero(Actor actor) {
        this.actors.add(actor);
        this.heroIndex = actors.indexOf(actor);
    }

    public void setUpMap() {
        setMapSize();

        map = new int[mapSize][mapSize];
        for (int i = 0; i < mapSize; i++) {
            for (int j = 0; j < mapSize; j++) {
                map[i][j] = 0;
            }
        }

        for (int i = 0; i < actors.size(); i++) {
            int x = actors.get(i).getLocation().getX();
            int y = actors.get(i).getLocation().getY();

            if (actors.get(i).isGoodOrEvil()) {
                map[y][x] = 1;
            } else {
                map[y][x] = 2;
            }
        }
    }

    public int[][] getMap() {
        return (map);
    }

    public Actor getActorAtLocation(int x, int y) {
        ArrayList<Actor> actors = mapModel.getActors();

        for (int i = 0; i < actors.size(); i++) {
            if (actors.get(i).getLocation().getX() == x) {
                if (actors.get(i).getLocation().getY() == y) {
                    return (actors.get(i));
                }
            }
        }
        return (null);
    }
}
