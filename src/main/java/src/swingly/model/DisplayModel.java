package src.swingly.model;

import lombok.Getter;
import lombok.Setter;
import src.swingly.ViewExit;
import src.swingly.controller.*;
import src.swingly.view.*;

import javax.swing.*;
import java.awt.*;
import java.util.Scanner;

public class DisplayModel {

    private static ViewExit exitView;

    MainFrameController         mainFrameController;
    NewGameModel                newGameModel;
    ContinueGameController      continueGameController;
    GameDisplayModel            gameDisplayModel;
    BattleFrameModel            battleFrameModel;

    Point                       location;

    @Setter
    @Getter
    boolean showGui;

    public DisplayModel() {
        mainFrameController = new MainFrameController(new MainFrameView());
        newGameModel = new NewGameModel(new NewGameController(), new NewGameView());
        continueGameController = new ContinueGameController(new ContinueGameView());
        gameDisplayModel = new GameDisplayModel(new GameDisplayController(), new GameDisplayView());
        battleFrameModel = new BattleFrameModel(new BattleFrameController(), new BattleFrameView());

        exitView = new ViewExit();

    }

    public void displayHideAll() {
        mainFrameController.hide();
        newGameModel.hide();
        continueGameController.hide();
        gameDisplayModel.hide();
        battleFrameModel.hide();
        exitView.setVisible(false);
    }

    public void displayMainMenu(Point location) {
        displayHideAll();
        this.location = location;

        mainFrameController.displayFrame(location);
    }

    public void displayBattleFrame(Point location) {
        displayHideAll();
        this.location = location;
        battleFrameModel.displayFrame(location);
    }

    public void displayBattleFrame() {
        displayHideAll();
        battleFrameModel.displayFrame(this.location);
    }

    public void displayGameAndMapFrame(Point location) {
        displayHideAll();
        this.location = location;
        gameDisplayModel.displayFrame(location);
    }

    public void displayGameAndMapFrame() {
        displayHideAll();
        gameDisplayModel.displayFrame(this.location);
    }

    public void displayNewGameScreen(Point location) {
        displayHideAll();

        this.location = location;
        newGameModel.displayFrame(location);
    }

    public void displayNewGameScreen() {
        displayHideAll();
        newGameModel.displayFrame(this.location);
    }

    public void displayContinueGameScreen(Point location) {
        displayHideAll();
        this.location = location;
        continueGameController.displayFrame(location);
    }

    public void displayContinueGameScreen() {
        displayHideAll();
        continueGameController.displayFrame(this.location);
    }

    public void displayMessage(String message) {
        if (showGui) {
            JOptionPane.showMessageDialog(null, message);
        } else {
            System.out.println(message);
        }
    }

    public String displayQuestion(String question) {
        Scanner scanner = new Scanner(System.in);
        String  answer;

        if (showGui) {
            switch(JOptionPane.showConfirmDialog(null, question)) {
                case (0): {
                    return "yes";
                }
                case (1): {
                    return "no";
                }
            }
            return "cancel";
        } else {
            System.out.println(question);
            System.out.print("> ");
            answer = scanner.nextLine();

            switch (answer) {
                case("exit"): {
                    System.exit(0);
                    break ;
                }
                case ("gui"): {
                    showGui = true;
                    break ;
                }
            }
            return answer;
        }
    }

    public boolean getShowGui() {
        return showGui;
    }

}
