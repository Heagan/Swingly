package src.swingly.model;

import src.swingly.controller.MapDisplayController;
import src.swingly.view.MapDisplayView;

import java.awt.*;

public class MapDisplayModel {
    MapDisplayController mapDisplayController;
    MapDisplayView mapDisplayView;

    public MapDisplayModel(MapDisplayController mapDisplayController, MapDisplayView mapDisplayView) {
        this.mapDisplayController = mapDisplayController;
        this.mapDisplayView = mapDisplayView;
    }

    public void hide() {
        mapDisplayView.setVisible(false);
    }

    public void displayFrame(Point location) {
        mapDisplayView.setLocation(location);
        mapDisplayView.setVisible(true);
    }
}
