package src.swingly.model;

import src.swingly.controller.BattleDisplayPanelController;
import src.swingly.controller.BattleFrameController;
import src.swingly.view.BattleFrameView;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static src.swingly.Swing.displayModel;

public class BattleFrameModel {
    BattleFrameController battleFrameController;
    BattleFrameView battleFrameView;

    BattleDisplayPanelModel playerPanel;
    BattleDisplayPanelModel enemyPanel;

    ButtonClickListener buttonClickListener = new ButtonClickListener();

    public BattleFrameModel(BattleFrameController battleFrameController, BattleFrameView battleFrameView) {
        this.battleFrameController = battleFrameController;
        this.battleFrameView = battleFrameView;

        this.battleFrameView.addButtonListeners(buttonClickListener);

        this.playerPanel = new BattleDisplayPanelModel(
                new BattleDisplayPanelController(),
                battleFrameView.getPlayerDisplayPnl());

        this.enemyPanel = new BattleDisplayPanelModel(
                new BattleDisplayPanelController(),
                battleFrameView.getEnemyDisplayPnl());
    }

    public void hide() {
        battleFrameView.setVisible(false);
    }

    public void displayFrame(Point location) {
        battleFrameController.setHeroAndEnemyVariables();
        battleFrameView.getTfBattleLog().setText(battleFrameController.getEnemyAppearedText());
        battleFrameView.getTfEnemyDetails().setText(battleFrameController.getEnemyDetails());
        battleFrameController.updateBattleInformationElements(playerPanel, enemyPanel);

        if (displayModel.getShowGui()) {
            battleFrameView.setLocation(location);
            battleFrameView.setVisible(true);
        } else {
            displayModel.displayMessage("BATTLE START!");
            displayModel.displayMessage(battleFrameController.getEnemyAppearedText());
            displayModel.displayMessage(battleFrameController.getEnemyDetails());
            if (displayModel.displayQuestion("Fight? y / n").contains("n")) {
                if (battleFrameController.flee()) {
                    displayModel.displayQuestion("You manage to flee successfully!");
                    displayModel.displayGameAndMapFrame();
                    return ;
                } else {
                    displayModel.displayQuestion("FAILED TO FLEE!\nGET READY TO FIGHT!");
                }
            }
            battleFrameController.startTextBattle();
        }
    }

    public class ButtonClickListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().contentEquals("Attack")) {
                battleFrameView.getTfBattleLog().append("----------------\n");
                battleFrameView.getTfBattleLog().append(battleFrameController.heroIssueAttack());
                battleFrameView.getTfBattleLog().append(battleFrameController.enemyIssueAttack());
                battleFrameController.updateBattleInformationElements(playerPanel, enemyPanel);
            }
            if (e.getActionCommand().contentEquals("Flee")) {
                if (battleFrameController.flee()) {
                    displayModel.displayGameAndMapFrame(battleFrameView.getLocation());
                } else {
                    battleFrameView.getTfBattleLog().append("FAILED TO FLEE!\n");
                    battleFrameView.getTfBattleLog().append(battleFrameController.enemyIssueAttack());
                    battleFrameView.getTfBattleLog().append(battleFrameController.enemyIssueAttack());
                    battleFrameController.updateBattleInformationElements(playerPanel, enemyPanel);
                }
            }
        }
    }

}
