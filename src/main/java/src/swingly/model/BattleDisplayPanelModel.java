package src.swingly.model;

import src.swingly.controller.BattleDisplayPanelController;
import src.swingly.view.BattleDisplayPanelView;

public class BattleDisplayPanelModel {
    BattleDisplayPanelController battleDisplayPanelController;
    BattleDisplayPanelView battleDisplayPanelView;

    public BattleDisplayPanelModel(BattleDisplayPanelController battleDisplayPanelController,
                                   BattleDisplayPanelView battleDisplayPanelView) {
        this.battleDisplayPanelController = battleDisplayPanelController;
        this.battleDisplayPanelView = battleDisplayPanelView;
    }

    public void updateInformation(String actorName, String actorStats, int pbValue) {
        battleDisplayPanelView.updateInformation(actorName, actorStats, pbValue);
    }
}
