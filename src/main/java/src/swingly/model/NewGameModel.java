package src.swingly.model;

import src.swingly.filemanagment.Savefile;
import src.swingly.controller.NewGameController;
import src.swingly.view.NewGameView;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import static src.swingly.Swing.*;

public class NewGameModel {
    NewGameController   newGameController;
    NewGameView         newGameView;

    ButtonClickListener buttonClickListener = new ButtonClickListener();
    FormTextChange      formTextChange = new FormTextChange();
    SliderListener      sliderListener = new SliderListener();

    public NewGameModel(NewGameController newGameController, NewGameView newGameView) {
        this.newGameController = newGameController;
        this.newGameView = newGameView;

        this.newGameView.addButtonListeners(buttonClickListener);
        this.newGameView.addSliderListener(sliderListener);
        this.newGameView.addDocumentListener(formTextChange);

        setViewHeroNameDefault();
    }

    void setViewHeroNameDefault() {
        ArrayList<String> names = actorModel.getNames();

        newGameView.getTfName().setText("Midori");
        newGameView.getTfName().setText(names.get((int) (Math.random() * names.size())));
    }

    public void hide() {
        newGameView.setVisible(false);
    }

    public void displayFrame(Point location) {
        if (displayModel.getShowGui()) {
            newGameView.setLocation(location);
            newGameView.setVisible(true);
            newGameView.getLblNewHero().setText(
                    newGameController.getMessage(newGameView.getTfName().getText(),
                    newGameController.getSliderClass(newGameView.getSbHeroClass().getValue())));
        } else {
            startCreateHero();
        }
    }

    String getClassFromQuestion(String answer) {
        switch (answer) {
            case ("w"): {
                return "Warrior";
            }
            case ("a"): {
                return "Archer";
            }
            case ("f"): {
                return "Fighter";
            }
        }
        return "";
    }

    private void startCreateHero() {
        String heroName;
        String heroClass;

        displayModel.displayMessage("Create New Hero:");
        do {
            heroName = displayModel.displayQuestion("Hero Name:");
        } while (heroName.length() <= 0);
        do {
            heroClass = getClassFromQuestion(displayModel.displayQuestion("Hero Class\nWarrior, Archer, Fighter\n(w | a | f):"));
        } while (!heroClass.contains("Warrior") && !heroClass.contains("Archer") && !heroClass.contains("Fighter"));
        displayModel.displayMessage(heroClass + " Selected");
        newGameController.createNewHero(heroName, heroClass);
        Savefile.saveGame();
        displayModel.displayMessage("Hero Created!");
        mapModel.setMapSize();
        actorModel.spawnEnemiesSquare();
        displayModel.displayGameAndMapFrame(newGameView.getLocation());
    }

    class FormTextChange implements DocumentListener {
        public void insertUpdate(DocumentEvent e) {
            newGameView.getLblNewHero().setText(
                    newGameController.getMessage(newGameView.getTfName().getText(),
                            newGameController.getSliderClass(newGameView.getSbHeroClass().getValue())));
        }

        public void removeUpdate(DocumentEvent e) {

        }

        public void changedUpdate(DocumentEvent e) {
        }
    }

    class SliderListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            JSlider source = (JSlider)e.getSource();
            if (!source.getValueIsAdjusting()) {
                newGameView.getLblNewHero().setText(
                        newGameController.getMessage(newGameView.getTfName().getText(),
                                newGameController.getSliderClass(newGameView.getSbHeroClass().getValue())));
            }
        }
    }

    public class ButtonClickListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();

            if (command.equals("CreateNewHero")) {
                newGameController.createNewHero(newGameView.getTfName().getText(),
                        newGameController.getSliderClass(newGameView.getSbHeroClass().getValue()));
                Savefile.saveGame();
                mapModel.setMapSize();
                actorModel.spawnEnemiesSquare();
                displayModel.displayGameAndMapFrame(newGameView.getLocation());
            } else if (command.equals("Back")) {
                displayModel.displayMainMenu(newGameView.getLocation());
            }
        }
    }
}
