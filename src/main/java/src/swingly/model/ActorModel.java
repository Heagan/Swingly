package src.swingly.model;

import lombok.Data;
import src.swingly.Actor;
import src.swingly.ActorFactory;
import src.swingly.Equipment;
import src.swingly.dependencies.Location;
import src.swingly.dependencies.Names;

import java.util.ArrayList;

import static src.swingly.Swing.mapModel;

@Data
public class ActorModel extends ActorFactory {
    public ArrayList<String> names;
    public static int knownMapSize = 0;

    public ActorModel() {
        getNameData();
    }

    public void refreshActorsStats(Actor actor) {
        setStats(actor);
        addEquipmentBonus(actor);
        setStatBonuses(actor);
    }

    public void getNameData() {
        names = Names.loadNames("./names.txt");
    }

    public void spawnEnemies() {
        ArrayList<Actor> actors = mapModel.getActors();
        Actor hero = mapModel.getHero();
        int mapSize;
        int level;
        int x = hero.getLocation().getX();
        int y = hero.getLocation().getY();
        int enemyCount;

        mapModel.setMapSize();
        mapSize = mapModel.getMapSize();
        enemyCount = (mapSize * mapSize) / 5;
        if (actors.size() > enemyCount) {
            return;
        }
        for (int i = 0; i < enemyCount - actors.size(); i++) {
            level = hero.getActorLevel();

            Actor enemy = createActor(names.get((int) (Math.random() * names.size())), getEnemyClass((int) (Math.random() * 3)),
                    level, 0, new Equipment());

            chanceCopyHeroEquipment(enemy);

            while (isLocationUnique(x, y) == false) {
                x = (int)(Math.random() * (mapSize));
                y = (int)(Math.random() * (mapSize));
            }
            enemy.setLocation(x, y);
            actors.add(enemy);
        }
        balanceEnemies();
    }

    public void spawnEnemiesSquare() {
        ArrayList<Actor> actors = mapModel.getActors();
        Actor hero = mapModel.getHero();
        int mapSize;
        int level;

        mapModel.setMapSize();
        mapSize = mapModel.getMapSize();
        for (int i = knownMapSize; i < mapSize; i++) {
            for (int j = knownMapSize; j < mapSize; j++) {
                if ((j % 2 == 0 && i % 2 == 0)) {
                    if (isLocationUnique(j, i)) {
                        level = hero.getActorLevel();
                        Actor enemy = createActor(names.get((int) (Math.random() * names.size())), getEnemyClass((int) (Math.random() * 3)),
                                level, 0, new Equipment(hero.getEquipment()));
                        enemy.setLocation(j, i);
                        actors.add(enemy);
                    }
                }
            }
        }
        knownMapSize = mapSize;
        balanceEnemies();
    }

    private String getEnemyClass(int enemyNumber) {
        switch (enemyNumber) {
            case (0): {
                return ("Shadow Hand");
            }
            case (1): {
                return ("Rabid Beast");
            }
            case (2): {
                return ("Bandit");
            }
            case (3): {
                return ("Demon");
            }
            case (4): {
                return ("Dragon");
            }
        }
        return ("Bandit");
    }

    void chanceCopyHeroEquipment(Actor actor) {
        Equipment equipment = new Equipment();
        Actor hero = mapModel.getHero();

        if (Math.random() > actor.getRandomFactor().getHandicap()) {
            equipment.setWeapon(hero.getEquipment().getWeapon());
        }
        if (Math.random() > actor.getRandomFactor().getHandicap()) {
            equipment.setArmor(hero.getEquipment().getArmor());
        }
        if (Math.random() > actor.getRandomFactor().getHandicap()) {
            equipment.setHelm(hero.getEquipment().getHelm());
        }

        actor.setEquipment(equipment);
    }

    public void balanceEnemies() {
        for (Actor enemy: mapModel.getActors()) {
            if (enemy.isGoodOrEvil()) {
                continue ;
            }

            chanceCopyHeroEquipment(enemy);
            refreshActorsStats(enemy);
        }
    }

    public void spawnBoss(int level) {
        Actor hero = mapModel.getHero();
        Actor boss = createActor("Evil " + hero.getActorName(), hero.getActorClass(),
                level, 0, hero.getEquipment());

        chanceCopyHeroEquipment(boss);

        boss.setBoss(true);
        mapModel.getActors().add(boss);
        boss.setLocation(new Location(hero.getLocation()));
        refreshActorsStats(boss);
    }

    public boolean isLocationUnique(int x, int y) {
        ArrayList<Actor> actors = mapModel.getActors();

        for (int i = 0; i < actors.size(); i++) {
            if (actors.get(i).getLocation().getX() == x) {
                if (actors.get(i).getLocation().getY() == y) {
                    return (false);
                }
            }
        }
        return (true);
    }
}
