package src.swingly;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static src.swingly.Swing.displayModel;

public class ViewExit extends JFrame {

    public JFrame f = new JFrame("GUI MODE");
    public JButton guiMode = new JButton("X");

    public void addButtonListeners() {
    }

    public ViewExit() {
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setLayout(new FlowLayout());
        f.add(new JLabel("Enable Gui"));
        f.add(guiMode);
        f.pack();

        guiMode.setActionCommand("switch");
        guiMode.addActionListener(new ButtonClickListener());

    }

    public class ButtonClickListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();

            if (command.equals("switch")) {
                displayModel.setShowGui(false);
            }
        }
    }

}
