package src.swingly;

import java.util.ArrayList;

public abstract class ActorFactory {
    ArrayList<String> defenceBonus = new ArrayList<String>();
    ArrayList<String> attackBonus = new ArrayList<String>();
    ArrayList<String> healthBonus = new ArrayList<String>();

    protected ActorFactory() {
        defenceBonus.add("Warrior");
        defenceBonus.add("Shadow Hand");

        attackBonus.add("Archer");
        attackBonus.add("Bandit");

        healthBonus.add("Fighter");
        healthBonus.add("Rabid Beast");
    }

    public Actor createActor(String name, String actorClass, int actorLevel, double actorExperience, Equipment equipment) {
        Actor actor = new Actor(name, actorClass, actorLevel, actorExperience, equipment);

        setStats(actor);
        addEquipmentBonus(actor);
        setStatBonuses(actor);
        return (actor);
    }

    protected Actor setStats(Actor actor) {
        int level = actor.getActorLevel();

        actor.setActorAttack(level);
        actor.setActorDefence(level);
        actor.setActorHealth(Math.min(50, level * 10));
        return (actor);
    }

    protected Actor setStatBonuses(Actor actor) {
       int level = actor.getActorLevel();

        if (defenceBonus.contains(actor.getActorClass())) {
            actor.setActorDefence((actor.getActorDefence() + level) * 2);
        }
        if (healthBonus.contains(actor.getActorClass())) {
            actor.setActorHealth((actor.getActorHealth() + (level * 10)));
        }
        if (attackBonus.contains(actor.getActorClass())) {
            actor.setActorAttack(actor.getActorAttack() + level);
        }
        return (actor);
    }

    protected Actor addEquipmentBonus(Actor actor) {
        int level = actor.getActorLevel();
        int multiplier;
        Equipment equipment = actor.getEquipment();

        multiplier = 1;
        if (equipment.getWeapon() != null) {
            Weapon weapon = equipment.getWeapon();

            if (weapon.getWeaponClass().contentEquals("Wooden")) {
                multiplier = 2;
            }
            if (weapon.getWeaponClass().contentEquals("Iron")) {
                multiplier = 3;
            }
            if (weapon.getWeaponClass().contentEquals("Diamond")) {
                multiplier = 4;
            }
            actor.setActorAttack(actor.getActorAttack() + (level * multiplier));
        }
        multiplier = 1;
        if (equipment.getArmor() != null) {
            Armor armor = equipment.getArmor();

            if (armor.getArmorClass().contentEquals("Wooden")) {
                multiplier = 2;
            }
            if (armor.getArmorClass().contentEquals("Iron")) {
                multiplier = 3;
            }
            if (armor.getArmorClass().contentEquals("Diamond")) {
                multiplier = 4;
            }
            actor.setActorDefence(actor.getActorDefence() + (level * multiplier));
        }
        multiplier = 1;
        if (equipment.getHelm() != null) {
            Helm helm = equipment.getHelm();

            if (helm.getHelmClass().contentEquals("Wooden")) {
                multiplier = 2;
            }
            if (helm.getHelmClass().contentEquals("Iron")) {
                multiplier = 3;
            }
            if (helm.getHelmClass().contentEquals("Diamond")) {
                multiplier = 4;
            }
            actor.setActorHealth(actor.getActorHealth() + (level * multiplier * 10));
        }
        return (actor);
    }
}
