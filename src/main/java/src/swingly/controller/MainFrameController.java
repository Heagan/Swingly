package src.swingly.controller;

import src.swingly.view.MainFrameView;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static src.swingly.Swing.displayModel;

public class MainFrameController {

    private ButtonClickListener buttonClickListener = new ButtonClickListener();
    private MainFrameView mainFrameView;

    public MainFrameController(MainFrameView mainFrameView) {
        this.mainFrameView = mainFrameView;
        this.mainFrameView.addButtonListener(buttonClickListener);
    }

    public void hide() {
        mainFrameView.setVisible(false);
    }

    public void displayFrame(Point location) {
        if (!displayModel.getShowGui()) {
            consoleStartGame();
            return ;
        }
        mainFrameView.setAlwaysOnTop(true);
        mainFrameView.setLocation(location);
        mainFrameView.setVisible(true);
    }

    private void consoleStartGame() {
        switch (displayModel.displayQuestion("New Game or Continue?\nn / c?")) {
            case ("n"): {
                displayModel.displayNewGameScreen();
                break ;
            }
            case ("c"): {
                displayModel.displayContinueGameScreen();
                break ;
            }
            default: {
                consoleStartGame();
            }
        }
    }

    public class ButtonClickListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();

            if (command.equals("Continue")) {
                displayModel.displayContinueGameScreen(mainFrameView.getLocation());
            }
            if (command.equals("New")) {
                displayModel.displayNewGameScreen(mainFrameView.getLocation());
            }
        }
    }

}
