/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.swingly.controller;

import lombok.Data;
import src.swingly.Actor;
import src.swingly.Equipment;
import src.swingly.dependencies.Location;

import java.util.ArrayList;

import static src.swingly.Swing.*;
import static src.swingly.Swing.mapModel;

@Data
public class GameDisplayController {
    private int map[][];
    private ArrayList<String> message = new ArrayList<String>();

    public GameDisplayController() {
        message.add("");
        message.add("...");
        message.add("Huh?");
        message.add("Fight, fight...");
        message.add("Anyone there?");
        message.add("Hello?");
        message.add("How far does this go?");
        message.add("I can do this!");
        message.add("I mustn't get lost now get lost now");
        message.add("What's that light over there?");
        message.add("Its pretty dark here...");
    }

    public String getHeroDetails() {
        String details = "";
        Actor actor = mapModel.getActors().get(mapModel.getHeroIndex());
        Equipment equipment = actor.getEquipment();

        actorModel.refreshActorsStats(actor);
        details = details.concat(actor.getActorName() + "\n");
        details = details.concat(actor.getActorClass() + "\n");
        details = details.concat("Lv: " + actor.getActorLevel() + "\n");
        details = details.concat("Exp: " + Math.round(actor.getActorExperience())+" \\ " +
                Math.round(actor.getRequiredExpForNextLevel()) + "\n");
        details = details.concat("Health: " + actor.getActorHealth() + "\n");
        details = details.concat("Attack: " + actor.getActorAttack() + "\n");
        details = details.concat("Defence: " + actor.getActorDefence() + "\n");
        details = details.concat("Weapon: " + equipment.getWeaponDetails() + "\n");
        details = details.concat("Armor: " + equipment.getArmorDetails() + "\n");
        details = details.concat("Helm: " + equipment.getHelmDetails() + "\n");

        return details;
    }

    public String refreshMapInfo() {
        Location heroLocation = mapModel.getHero().getLocation();
        String mapLine = "";
        String mapText;
        int mapSize;
        int horLineLength = 70;
        int verLineLength = 33;

        int verStart;
        int horStart;

        mapModel.setUpMap();
        map = mapModel.getMap();
        mapSize = mapModel.getMapSize();

        horStart = 0;
        verStart = 0;
        if (mapSize * 2 > horLineLength) {
            if (heroLocation.getX() > mapSize / 2) {
                horStart = heroLocation.getX() - horLineLength / 4;
            } else {
                horStart = heroLocation.getX() - horLineLength / 2;
            }
            verStart = heroLocation.getY() - verLineLength / 2;
            if (horStart < 0) {
                horStart = 0;
            }
            if (verStart < 0) {
                verStart = 0;
            }
        }

        mapText = "";
        if (displayModel.getShowGui()) {
            for (int verBnkLns = 0;verBnkLns < (verLineLength / 2 - heroLocation.getY()); verBnkLns++) {
                mapLine += "\n";
            }
        }
        mapText = mapText.concat(mapLine);
        mapLine = "";
        for (int i = verStart; i < mapSize; i++) {
            if (displayModel.getShowGui()) {
                for (int horBnkLns = 0; horBnkLns < (horLineLength / 2 - heroLocation.getX() * 2); horBnkLns++) {
                    mapLine = mapLine.concat(" ");
                }
            }
            for (int j = horStart; j < mapSize; j++) {
                if (map[i][j] == 0) {
                    mapLine = mapLine.concat(" ");
                } else if (map[i][j] == 1) {
                    mapLine = mapLine.concat("O");
                } else if (map[i][j] == 2) {
                    int textLevel = mapModel.getActorAtLocation(j, i).getActorLevel() - mapModel.getHero().getActorLevel() + 1;

                    if (textLevel > 9) {
                        mapLine = mapLine.concat("~");
                    } else if (textLevel > 1) {
                        mapLine = mapLine.concat("#");
                    } else {
                        mapLine = mapLine.concat("+");
                    }
                }
                mapLine = mapLine.concat(" ");
            }
            mapLine = mapLine.concat("\n");
            mapText = mapText.concat(mapLine);
            mapLine = "";
        }
        return mapText;
    }

    public void startBossBattle() {
        if (displayModel.displayQuestion("BOSS BATTLE!\nYOU CANNOT FLEE\nARE YOU READY?").contains("y")) {
            actorModel.spawnBoss(10);
            mapModel.setEnemyIndexFromActor(
                    mapModel.getEnemyAtLocation(
                            mapModel.getHero().getLocation()));
            displayModel.displayBattleFrame();
        }
    }

    public void appendLocationDirection(String direction, Location location) {

        if (direction.equals("Up")) {
            location.appendLocation(0, -1);
        }
        if (direction.equals("Down")) {
            location.appendLocation(0, 1);
        }
        if (direction.equals("Left")) {
            location.appendLocation(-1, 0);
        }
        if (direction.equals("Right")) {
            location.appendLocation(1, 0);
        }
    }

    public boolean moveEvents(String command) {
        Actor hero = mapModel.getHero();
        Location location = new Location(hero.getLocation());

        if ((command.equals("Up") ||
                command.equals("Down")) ||
                (command.equals("Left") ||
                        command.equals("Right"))) {
            //move hero
            appendLocationDirection(command, location);
            //outside map?
            if (mapModel.isLocationInMap(location)) {
                //is there an enemy on you?
                if (mapModel.shouldStartBattle(location)) {
                    //save reference to enemy at this position
                    mapModel.setEnemyIndexFromActor(mapModel.getEnemyAtLocation(location));
                    //start battle
                    return true;
                } else {
                    //if there's no need to fight then move forward!
                    hero.setLocation(location);
                }
            } else {
                startBossBattle();
            }
        }
        return false;
    }

    public void moveEnemies() {
        for (Actor actor: mapModel.getActors()) {
            if (!actor.isGoodOrEvil()) {
                actor.moveRandom();
                setLevelRelativeToLocation(actor);
            }
        }
    }

    public void setLevelRelativeToLocation(Actor actor) {
        // lvl - 1 * 5 + 10 - lvl % 2
        int lvl = 0;
        int result = 1;
        int mapSize = mapModel.getMapSize();
        Location loc = new Location(actor.getLocation().getX(), actor.getLocation().getY());

        //Don't change the boss level
        //Might change this is the future
        if (actor.isBoss()) {
            return ;
        }

        loc.setX(Math.abs(loc.getX() - mapSize / 2));
        loc.setY(Math.abs(loc.getY() - mapSize / 2));

        while (result < loc.getX() * 2 && result < loc.getY() * 2) {
            result = (lvl - 1) * 5 + 10 - (lvl % 2);
            if (result < loc.getX() * 2 && result < loc.getY() * 2) {
                lvl++;
            }
        }

        lvl += mapModel.getHero().getActorLevel();
        actor.setActorLevel(lvl);
    }
}
