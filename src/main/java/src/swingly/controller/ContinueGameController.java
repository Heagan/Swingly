/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.swingly.controller;

import src.swingly.Actor;
import src.swingly.Equipment;
import src.swingly.Swing;
import src.swingly.filemanagment.Savefile;
import src.swingly.view.ContinueGameView;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import static src.swingly.Swing.actorModel;
import static src.swingly.Swing.displayModel;
import static src.swingly.Swing.mapModel;

public class ContinueGameController {

    private ContinueGameView            continueGameView;

    private SaveDisplayPnlController    saveDisplayPanel1;
    private SaveDisplayPnlController    saveDisplayPanel2;
    private SaveDisplayPnlController    saveDisplayPanel3;


    public ContinueGameController(ContinueGameView continueGameView) {
        this.continueGameView = continueGameView;

        this.continueGameView.addButtonListener(new ButtonClickListener());

        this.saveDisplayPanel1 = new SaveDisplayPnlController(this.continueGameView.getSaveDisplayPnlView1());
        this.saveDisplayPanel2 = new SaveDisplayPnlController(this.continueGameView.getSaveDisplayPnlView2());
        this.saveDisplayPanel3 = new SaveDisplayPnlController(this.continueGameView.getSaveDisplayPnlView3());
    }

    private Actor processSaveData(String data[]) {
        Equipment equipment = new Equipment();
        Actor actor;

        if (data.length != 7) {
            return null;
        }

        equipment.addWeapon(data[4].trim());
        equipment.addArmor(data[5].trim());
        equipment.addHelm(data[6].trim());

        actor = Swing.actorModel.createActor(data[0], data[1],
                Integer.parseInt(data[2]), Double.parseDouble(data[3]),
                equipment);

        return actor;
    }

    public void hide() {
        continueGameView.setVisible(false);
    }

    public void displayFrame(Point location) {
        if (!displayModel.getShowGui()) {
            consoleContinueGameSetup();
            return ;
        }
        continueGameView.setLocation(location);
        continueGameView.setVisible(true);
        loadSaveDataIntoPanels();
    }

    private void loadPanelInfo(String data[]) {
        setPnlSaveSlots(processSaveData(data));
    }

    private void loadSaveDataIntoPanels() {
        ArrayList<String[]> data = new ArrayList<String[]> ();

        Savefile.loadSaveData(data);
        if (data.size() > 0) {
            loadPanelInfo(data.get(0));
        }
        if (data.size() > 1) {
            loadPanelInfo(data.get(1));
        }
        if (data.size() > 2) {
            loadPanelInfo(data.get(2));
        }
    }

    private void setPnlSaveSlots(Actor actor) {

        if (!saveDisplayPanel1.isAssignedDetails()) {
            saveDisplayPanel1.changePanelDetails(actor);
        } else if (!saveDisplayPanel2.isAssignedDetails()) {
            saveDisplayPanel2.changePanelDetails(actor);
        } else if (!saveDisplayPanel3.isAssignedDetails()) {
            saveDisplayPanel3.changePanelDetails(actor);
        }
    }

    public class ButtonClickListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();

            System.out.println(command);

            if (command.equals("Back")) {
                displayModel.displayMainMenu(continueGameView.getLocation());
            }
        }
    }

    private void consoleContinueGameSetup() {
        Actor hero = loadSaveDataInput();

        if (hero == null) {
            return ;
        }
        createSavedHero(hero);
        displayModel.displayMessage("Hero Loaded Successfully!");
        displayModel.displayGameAndMapFrame();
    }

    private void createSavedHero(Actor hero) {
        mapModel.addHero(hero);
        mapModel.setMapSize();
        mapModel.getHero().setLocation(mapModel.getMapSize() / 2, mapModel.getMapSize() / 2);

        actorModel.spawnEnemiesSquare();
    }

    private Actor loadSaveDataInput() {
        ArrayList<String[]> data = new ArrayList<String[]>();
        Actor savedActor;

        Savefile.loadSaveData(data);
        if (data.size() == 0) {
            displayModel.displayMessage("No hero to load!");
            if (displayModel.displayQuestion("Create a new hero?\ny / n").equals("y")) {
                displayModel.displayNewGameScreen();
            }
            return loadSaveDataInput();
        }

        savedActor = processSaveData(data.get(0));
        while (!displayModel.displayQuestion("Load Hero: " + savedActor.getActorName() + "?\ny / n").contains("y")) {
            data.remove(0);
            if (data.size() > 0) {
                savedActor = processSaveData(data.get(0));
            } else {
                if (displayModel.displayQuestion("Create a new hero?\ny / n").equals("y")) {
                    displayModel.displayNewGameScreen();
                    return null;
                } else {
                    return loadSaveDataInput();
                }
            }
        }
        return savedActor;
    }

}
