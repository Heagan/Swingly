package src.swingly.controller;

import lombok.Data;
import src.swingly.Actor;
import src.swingly.Equipment;
import src.swingly.model.BattleDisplayPanelModel;
import src.swingly.filemanagment.Savefile;

import javax.swing.*;

import static src.swingly.Swing.actorModel;
import static src.swingly.Swing.displayModel;
import static src.swingly.Swing.mapModel;

@Data
public class BattleFrameController extends JFrame {

    private Actor hero;
    private Actor enemy;
    private double heroHealth;
    private double enemyHealth;

    public void updateBattleInformationElements(BattleDisplayPanelModel playerPanel,
                                                BattleDisplayPanelModel enemyPanel) {
        String  actorStats;
        int     actorHealthPercentage;

        actorStats = "Lvl:" + hero.getActorLevel() + " " + Math.round(heroHealth) + "/" + hero.getActorHealth() + " A:" + hero.getActorAttack() + " D:" + hero.getActorDefence();
        actorHealthPercentage = 100 - ((int) (heroHealth / hero.getActorHealth() * 100));
        playerPanel.updateInformation(hero.getActorName(), actorStats, actorHealthPercentage);

        actorStats = "Lvl:" + enemy.getActorLevel() + " " + Math.round(enemyHealth) + "/" + enemy.getActorHealth() + " A:" + enemy.getActorAttack() + " D:" + enemy.getActorDefence();
        actorHealthPercentage = 100 - ((int) (enemyHealth / enemy.getActorHealth() * 100));
        enemyPanel.updateInformation(enemy.getActorName(), actorStats, actorHealthPercentage);
    }

    public void setHeroAndEnemyVariables() {
        hero = mapModel.getHero();
        heroHealth = hero.getActorHealth();
        enemy = mapModel.getActors().get(mapModel.getEnemyIndex());
        enemyHealth = enemy.getActorHealth();
    }

    public String getEnemyAppearedText() {
        return("Lvl:" + enemy.getActorLevel() + " Enemy " + enemy.getActorName() + " has appeared!\n");
    }

    public String getEnemyDetails() {
        String enemyDetails;

        enemyDetails = "";
        enemyDetails = enemyDetails.concat("Lvl:" + enemy.getActorLevel() + " " + enemy.getActorClass() + " " + enemy.getActorName() + "\n");
        enemyDetails = enemyDetails.concat("H:" + enemyHealth +
                " A:" + (int)enemy.getActorAttack() +
                " D:" + (int)enemy.getActorDefence() + "\n");
        enemyDetails = enemyDetails.concat(enemy.getEquipment().getWeaponDetails() + "\n");
        enemyDetails = enemyDetails.concat(enemy.getEquipment().getArmorDetails() + "\n");
        enemyDetails = enemyDetails.concat(enemy.getEquipment().getHelmDetails() + "\n");

        return enemyDetails;
    }

    public void dropItem() {
        int itemChance = (int) (Math.random() * 3);
        String item = "";
        String quality = "";
        String itemName[] = {"Hizah", "Shamuk", "Revrag", "Eagle",
                "Bane", "Thorn", "Rapier", "Malnior", "Aegis"};
        Equipment equipment = hero.getEquipment();

        if (enemy.getRandomFactor().getDropChance() > 0.5) {
            if (itemChance == 0) {
                quality = equipment.getQuality(equipment.getWeaponDetails());
                item = " Sword " + itemName[(int) (Math.random() * itemName.length)];
            }
            if (itemChance == 1) {
                quality = equipment.getQuality(equipment.getArmorDetails());
                item = " Armor " + itemName[(int) (Math.random() * itemName.length)];
            }
            if (itemChance == 2) {
                quality = equipment.getQuality(equipment.getHelmDetails());
                item = " Helm " + itemName[(int) (Math.random() * itemName.length)];
            }
            quality = equipment.getNextBestQuality(quality);
            if (displayModel.displayQuestion("You found a \"" + quality + " " + item + "\"\nEquip?!").equals("yes")) {
                if (itemChance == 0) {
                    hero.getEquipment().addWeapon(item + quality);
                }
                if (itemChance == 1) {
                    hero.getEquipment().addArmor(item + quality);
                }
                if (itemChance == 2) {
                    hero.getEquipment().addHelm(item + quality);
                }
            }
        } else {
            displayModel.displayMessage( "You didn't find any item...");
        }
    }

    public void lostBattle() {
        displayModel.displayMessage("YOU HAVE BEEN DEFEATED!\nGAME OVER");
        System.exit(1);
        displayModel.displayGameAndMapFrame();
    }

    public void wonBattle() {
        int level = enemy.getActorLevel();
        int xp = (int) ((int)((Math.pow(level, 2) * (100 - level)) / level) * ((1 * (enemy.getActorAttack() / hero.getActorAttack())) + 1) * ((2 * (enemy.getActorDefence() / hero.getActorDefence()))) + 1);

        mapModel.getActors().remove(mapModel.getEnemyIndex());

        if (enemy.isBoss()) {
            displayModel.displayMessage("YOU HAVE WON THE GAME!");
            System.exit(1);
        } else {
            hero.setLocation(enemy.getLocation().getX(), enemy.getLocation().getY());
            displayModel.displayMessage("Enemy Defeated!\nGained xp: " + xp);
            hero.appendExperience(xp);
            dropItem();
        }

        actorModel.refreshActorsStats(hero);
        actorModel.spawnEnemies();
        Savefile.saveGame();
        displayModel.displayGameAndMapFrame();
    }

    public String heroIssueAttack() {
        String battleLog;
        double damage;
        double critChance; //its random everytime its called, I need to use it in 2 places
        double enemyDefence = enemy.getActorDefence();

        battleLog = "";
        //(0.06*Armor)/(1+0.06*Armor) dota 2 armor attack reduction formula
        critChance = hero.getRandomFactor().getCritChance();
        damage = hero.getActorAttack() - hero.getActorAttack() * ((0.05 * enemyDefence)/(1 + 0.05 * enemyDefence));
        if (damage < 1) {
            damage = 1;
        }
        if (critChance > 0.5) {
            damage *= 2.0 - critChance; // 2 - 0.7 = 30% crit
            battleLog = battleLog.concat("CRIT!" + "\n");
        }
        enemyHealth = Math.max(0, enemyHealth - damage);
        damage = (double)(Math.round(damage * 100)) / 100;
        battleLog = battleLog.concat("You dealt:" + damage + "\n");

        return battleLog;
    }

    public void startTextBattle() {
        while (heroHealth > 0 && enemyHealth > 0) {
            displayModel.displayMessage(heroIssueAttack());
            displayModel.displayMessage(enemyIssueAttack());
        }
    }

    public String enemyIssueAttack() {
        String battleLog;
        double damage;
        double critChance; //its random everytime its called, I need to use it in 2 places
        double heroDefence = hero.getActorDefence();
        battleLog = "";

        critChance = enemy.getRandomFactor().getCritChance();
        if (enemyHealth <= 0) {
            wonBattle();
            return battleLog;
        }

        damage = enemy.getActorAttack() - enemy.getActorAttack() * ((0.05 * heroDefence)/(1 + 0.05 * heroDefence));
        if (damage < 1) {
            damage = 1;
        }
        if (critChance > 0.5) {
            damage *= 2.0 - critChance; // 2 - 0.7 = 30% crit
            battleLog = battleLog.concat("CRIT!\n");
        }
        heroHealth = Math.max(0, heroHealth - damage);

        //round off 2 decimal places
        damage = (double)(Math.round(damage * 100)) / 100;
        battleLog = battleLog.concat("They dealt:" + damage + "\n");
        if (heroHealth <= 0) {
            lostBattle();
        }
        return battleLog;
    }

    public boolean flee() {
        if (Math.random() > 0.5) {
            if (enemy.isBoss()) {
                displayModel.displayMessage( "He watches your every move!\nYou cannot flee!");
                return false;
            } else {
                displayModel.displayGameAndMapFrame();
                return true;
            }
        } else {
            return false;
        }
    }
}
