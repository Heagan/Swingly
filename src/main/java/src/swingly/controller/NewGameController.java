/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.swingly.controller;

import src.swingly.Actor;
import src.swingly.Equipment;

import javax.swing.*;

import static src.swingly.Swing.actorModel;
import static src.swingly.Swing.mapModel;

public class NewGameController extends JFrame {
    public NewGameController() {

    }

    public String getMessage(String playerName, String selectedClass) {
        return playerName + " the " + selectedClass;
    }

    public String getSliderClass(int sliderValue) {
        if (sliderValue < 33) {
            return ("Archer");
        } if (sliderValue < 75) {
            return ("Warrior");
        } else {
            return ("Fighter");
        }

    }

    public void createNewHero(String heroName, String heroClass) {
        Actor  hero;

        hero = actorModel.createActor(heroName, heroClass, 1, 0, new Equipment());
        mapModel.addHero(hero);
        mapModel.setMapSize();
        mapModel.getHero().setLocation(mapModel.getMapSize() / 2, mapModel.getMapSize() / 2);
    }
}
