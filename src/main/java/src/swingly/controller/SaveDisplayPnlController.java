package src.swingly.controller;

import lombok.Data;
import src.swingly.Actor;
import src.swingly.view.SaveDisplayPnlView;

import java.awt.event.MouseListener;

import static src.swingly.Swing.actorModel;
import static src.swingly.Swing.displayModel;
import static src.swingly.Swing.mapModel;

@Data
public class SaveDisplayPnlController {
    private boolean     isAssignedDetails = false;
    private Actor       actorDetails;

    SaveDisplayPnlView  saveDisplayPnlView;
    MouseListener       mouseListener = new java.awt.event.MouseAdapter() {
        public void mouseClicked(java.awt.event.MouseEvent evt) {
            formMouseClicked(evt);
        }
    };

    public SaveDisplayPnlController(SaveDisplayPnlView saveDisplayPnlView) {
        this.saveDisplayPnlView = saveDisplayPnlView;
        this.saveDisplayPnlView.addListener(mouseListener);
    }

    public void changePanelDetails(Actor actor) {
        saveDisplayPnlView.changeDetails(actor);
        isAssignedDetails = true;
        actorDetails = actor;
    }

    private void formMouseClicked(java.awt.event.MouseEvent evt) {
        if (!isAssignedDetails) {
            return ;
        }

        mapModel.addHero(actorDetails);
        mapModel.setMapSize();
        mapModel.getHero().setLocation(mapModel.getMapSize() / 2, mapModel.getMapSize() / 2);

        actorModel.spawnEnemiesSquare();
        displayModel.displayGameAndMapFrame();
    }



}
