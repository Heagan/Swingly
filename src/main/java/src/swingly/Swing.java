package src.swingly;

import src.swingly.model.ActorModel;
import src.swingly.model.DisplayModel;
import src.swingly.model.MapModel;

import java.awt.*;

public class Swing {
    public static MapModel      mapModel;
    public static ActorModel    actorModel;
    public static DisplayModel  displayModel;

    public static void main(String[] args) {

            mapModel        = new MapModel();
            actorModel      = new ActorModel();
            displayModel    = new DisplayModel();

        if (args.length > 0) {
            if (args[0].equals("gui")) {
                displayModel.setShowGui(true);
            } else if (args[0].equals("console")) {
                displayModel.setShowGui(false);
            }
        } else {
            displayModel.setShowGui(true);
        }
        displayModel.displayMainMenu(new Point(500, 200));
    }

    public static void Error(String message) {
        System.out.println(message);
        System.exit(-1);
    }
}


