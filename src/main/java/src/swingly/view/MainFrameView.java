package src.swingly.view;

import lombok.Data;

import javax.swing.*;
import java.awt.event.ActionListener;

@Data
public class MainFrameView extends JFrame {

    public MainFrameView() {
        initComponents();
        setButtonsActionCommands();
    }

    void setButtonsActionCommands() {
        btnNewGame.setActionCommand("New");
        btnContinue.setActionCommand("Continue");
    }

    public void addButtonListener(ActionListener buttonClickListener) {
        btnNewGame.addActionListener(buttonClickListener);
        btnContinue.addActionListener(buttonClickListener);
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        pnlMain = new javax.swing.JPanel();
        btnContinue = new javax.swing.JButton();
        btnNewGame = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Swingly");
        setName("mainFrameView"); // NOI18N
        setPreferredSize(new java.awt.Dimension(500, 500));
        setResizable(false);

        pnlMain.setBackground(new java.awt.Color(51, 51, 51));

        btnContinue.setFont(new java.awt.Font("Courier New", 1, 48)); // NOI18N
        btnContinue.setText("Continue");

        btnNewGame.setFont(new java.awt.Font("Courier New", 1, 48)); // NOI18N
        btnNewGame.setText("New Game");

        lblTitle.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/title.png"));

        javax.swing.GroupLayout pnlMainLayout = new javax.swing.GroupLayout(pnlMain);
        pnlMain.setLayout(pnlMainLayout);
        pnlMainLayout.setHorizontalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainLayout.createSequentialGroup()
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlMainLayout.createSequentialGroup()
                        .addGap(120, 120, 120)
                        .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnContinue, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnNewGame, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlMainLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(lblTitle)))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        pnlMainLayout.setVerticalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMainLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnNewGame, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnContinue, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(81, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }

    private JButton btnContinue;
    private JButton btnNewGame;
    private JLabel lblTitle;
    private JPanel pnlMain;
}
