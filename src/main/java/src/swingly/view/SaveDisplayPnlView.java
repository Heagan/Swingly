package src.swingly.view;

import lombok.Data;
import src.swingly.Actor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;

@Data
public class SaveDisplayPnlView extends JPanel {

    public SaveDisplayPnlView() {
        initComponents();
    }

    public void addListener(MouseListener mouseListener) {
        this.addMouseListener(mouseListener);
    }

    public void changeDetails(Actor actor) {
        lblHeroName.setText(actor.getActorName());
        lblHeroClass.setText(actor.getActorClass());
        lblHeroLevel.setText(String.valueOf(actor.getActorLevel()));
    }

    private void initComponents() {

        pnlDetails = new JPanel();
        lblHeroName = new JLabel();
        lblHeroClass = new JLabel();
        lblHeroLevel = new JLabel();
        pnlIcons = new JPanel();
        lblArmorIcon = new JLabel();
        lblWeaponIcon = new JLabel();
        lblHelmIcon = new JLabel();

        setBackground(new Color(51, 51, 51));
        setForeground(new Color(51, 51, 51));

        pnlDetails.setOpaque(false);

        lblHeroName.setBackground(javax.swing.UIManager.getDefaults().getColor("List.dropLineColor"));
        lblHeroName.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        lblHeroName.setForeground(new java.awt.Color(255, 255, 255));
        lblHeroName.setText("Hero Name");

        lblHeroClass.setBackground(javax.swing.UIManager.getDefaults().getColor("List.dropLineColor"));
        lblHeroClass.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        lblHeroClass.setForeground(new java.awt.Color(255, 255, 255));
        lblHeroClass.setText("Hero Class");

        lblHeroLevel.setBackground(javax.swing.UIManager.getDefaults().getColor("List.dropLineColor"));
        lblHeroLevel.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        lblHeroLevel.setForeground(new java.awt.Color(255, 255, 255));
        lblHeroLevel.setText("Hero Level");
        lblHeroLevel.setToolTipText("");

        javax.swing.GroupLayout pnlDetailsLayout = new javax.swing.GroupLayout(pnlDetails);
        pnlDetails.setLayout(pnlDetailsLayout);
        pnlDetailsLayout.setHorizontalGroup(
            pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblHeroName)
                    .addComponent(lblHeroClass)
                    .addComponent(lblHeroLevel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlDetailsLayout.setVerticalGroup(
            pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDetailsLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblHeroName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblHeroClass, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblHeroLevel, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlIcons.setOpaque(false);

        lblArmorIcon.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/icons/Diamond.png")); // NOI18N
        lblWeaponIcon.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/icons/H.png")); // NOI18N
        lblHelmIcon.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/icons/I.png")); // NOI18N

        javax.swing.GroupLayout pnlIconsLayout = new javax.swing.GroupLayout(pnlIcons);
        pnlIcons.setLayout(pnlIconsLayout);
        pnlIconsLayout.setHorizontalGroup(
            pnlIconsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlIconsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblArmorIcon)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblWeaponIcon)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblHelmIcon)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlIconsLayout.setVerticalGroup(
            pnlIconsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlIconsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlIconsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblHelmIcon)
                    .addComponent(lblArmorIcon)
                    .addComponent(lblWeaponIcon))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(pnlDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlIcons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pnlDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(pnlIcons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }

    private JLabel lblArmorIcon;
    private JLabel lblHelmIcon;
    private JLabel lblHeroClass;
    private JLabel lblHeroLevel;
    private JLabel lblHeroName;
    private JLabel lblWeaponIcon;
    private JPanel pnlDetails;
    private JPanel pnlIcons;
}
