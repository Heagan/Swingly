package src.swingly.view;

import lombok.Data;

import javax.swing.*;
import java.awt.event.ActionListener;

@Data
public class BattleFrameView extends JFrame {
    public BattleFrameView() {
        initComponents();

        tfEnemyDetails.setEditable(false);
        tfBattleLog.setEditable(false);
    }

    public void addButtonListeners(ActionListener buttonClickListener) {
        btnAttack.addActionListener(buttonClickListener);
        btnFlee.addActionListener(buttonClickListener);
    }

    private void initComponents() {

        pnlMain = new javax.swing.JPanel();
        pnlBatlle = new javax.swing.JPanel();
        playerDisplayPnl = new BattleDisplayPanelView();
        enemyDisplayPnl = new BattleDisplayPanelView();
        btnAttack = new javax.swing.JButton();
        btnFlee = new javax.swing.JButton();
        spED = new javax.swing.JScrollPane();
        tfEnemyDetails = new javax.swing.JTextArea();
        spLog = new javax.swing.JScrollPane();
        tfBattleLog = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Swingly");
        setName("battleFrame"); // NOI18N
        setResizable(false);
        setSize(new java.awt.Dimension(600, 500));

        pnlMain.setBackground(new java.awt.Color(51, 51, 51));
        pnlMain.setPreferredSize(new java.awt.Dimension(600, 500));

        pnlBatlle.setBackground(new java.awt.Color(255, 255, 255));
        javax.swing.GroupLayout pnlBatlleLayout = new javax.swing.GroupLayout(pnlBatlle);
        pnlBatlle.setLayout(pnlBatlleLayout);
        pnlBatlleLayout.setHorizontalGroup(
            pnlBatlleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBatlleLayout.createSequentialGroup()
                .addContainerGap(1, Short.MAX_VALUE)
                .addComponent(playerDisplayPnl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(pnlBatlleLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(enemyDisplayPnl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlBatlleLayout.setVerticalGroup(
            pnlBatlleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBatlleLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(enemyDisplayPnl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 151, Short.MAX_VALUE)
                .addComponent(playerDisplayPnl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnAttack.setFont(new java.awt.Font("Courier New", 1, 24)); // NOI18N
        btnAttack.setText("Attack");

        btnFlee.setFont(new java.awt.Font("Courier New", 1, 24)); // NOI18N
        btnFlee.setText("Flee");

        tfEnemyDetails.setColumns(20);
        tfEnemyDetails.setRows(5);
        tfEnemyDetails.setAutoscrolls(false);
        spED.setViewportView(tfEnemyDetails);

        tfBattleLog.setColumns(20);
        tfBattleLog.setRows(5);
        tfBattleLog.setAutoscrolls(false);
        spLog.setViewportView(tfBattleLog);

        javax.swing.GroupLayout pnlMainLayout = new javax.swing.GroupLayout(pnlMain);
        pnlMain.setLayout(pnlMainLayout);
        pnlMainLayout.setHorizontalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMainLayout.createSequentialGroup()
                        .addComponent(pnlBatlle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spLog, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(pnlMainLayout.createSequentialGroup()
                        .addComponent(btnAttack, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFlee, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spED, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(27, Short.MAX_VALUE))
        );
        pnlMainLayout.setVerticalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlBatlle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(spLog))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(spED, javax.swing.GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)
                    .addComponent(btnAttack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnFlee, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(28, 28, 28))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }
    
    private JButton btnAttack;
    private JButton btnFlee;
    private BattleDisplayPanelView playerDisplayPnl;
    private BattleDisplayPanelView enemyDisplayPnl;
    private JPanel pnlBatlle;
    private JPanel pnlMain;
    private JScrollPane spED;
    private JScrollPane spLog;
    private JTextArea tfBattleLog;
    private JTextArea tfEnemyDetails;
}
