package src.swingly.view;

import lombok.Data;

import javax.swing.*;
import java.awt.event.ActionListener;

@Data
public class ContinueGameView extends JFrame {

    public ContinueGameView() {
        initComponents();
    }

    public void addButtonListener(ActionListener buttonClickListener){
        btnBack.addActionListener(buttonClickListener);
    }

    private void initComponents() {

        pnlMain = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();
        pnlSaveSlots = new javax.swing.JPanel();
        saveDisplayPnlView1 = new SaveDisplayPnlView();
        saveDisplayPnlView2 = new SaveDisplayPnlView();
        saveDisplayPnlView3 = new SaveDisplayPnlView();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Swingly");
        setName("continueFrame"); // NOI18N
        setResizable(false);
        setSize(new java.awt.Dimension(500, 500));

        pnlMain.setBackground(new java.awt.Color(51, 51, 51));
        pnlMain.setPreferredSize(new java.awt.Dimension(500, 500));

        btnBack.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        btnBack.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonLeft.png")); // NOI18N
        btnBack.setActionCommand("Back");
        btnBack.setContentAreaFilled(false);
        btnBack.setPreferredSize(new java.awt.Dimension(75, 75));
        btnBack.setPressedIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonLeftSelected.png")); // NOI18N

        pnlSaveSlots.setBackground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout pnlSaveSlotsLayout = new javax.swing.GroupLayout(pnlSaveSlots);
        pnlSaveSlots.setLayout(pnlSaveSlotsLayout);
        pnlSaveSlotsLayout.setHorizontalGroup(
            pnlSaveSlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSaveSlotsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSaveSlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSaveSlotsLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(saveDisplayPnlView1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlSaveSlotsLayout.createSequentialGroup()
                        .addGroup(pnlSaveSlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(saveDisplayPnlView2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(saveDisplayPnlView3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlSaveSlotsLayout.setVerticalGroup(
            pnlSaveSlotsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSaveSlotsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(saveDisplayPnlView1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(saveDisplayPnlView2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(saveDisplayPnlView3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlMainLayout = new javax.swing.GroupLayout(pnlMain);
        pnlMain.setLayout(pnlMainLayout);
        pnlMainLayout.setHorizontalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlSaveSlots, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(111, Short.MAX_VALUE))
        );
        pnlMainLayout.setVerticalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnlSaveSlots, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(80, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(pnlMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(pnlMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }

    private JButton btnBack;
    private JPanel pnlMain;
    private JPanel pnlSaveSlots;

    private SaveDisplayPnlView saveDisplayPnlView1;
    private SaveDisplayPnlView saveDisplayPnlView2;
    private SaveDisplayPnlView saveDisplayPnlView3;
}
