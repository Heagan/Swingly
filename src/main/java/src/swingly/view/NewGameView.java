package src.swingly.view;

import lombok.Data;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import java.awt.event.ActionListener;

@Data
public class NewGameView extends JFrame {

    public NewGameView() {
        initComponents();
        setActionCommands();
    }

    public void setActionCommands() {
        btnContinue.setActionCommand("CreateNewHero");
    }

    public void addDocumentListener(DocumentListener documentListener) {
        tfName.getDocument().addDocumentListener(documentListener);
    }

    public void addSliderListener(ChangeListener sliderListener) {
        sbHeroClass.addChangeListener(sliderListener);
    }

    public void addButtonListeners(ActionListener buttonClickListener) {
        btnContinue.addActionListener(buttonClickListener);
        btnBack.addActionListener(buttonClickListener);
    }

    private void initComponents() {

        pnlMain = new javax.swing.JPanel();
        sbHeroClass = new javax.swing.JSlider();
        lblHeros = new javax.swing.JLabel();
        pnlButtons = new javax.swing.JPanel();
        tfName = new javax.swing.JTextField();
        btnContinue = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        lblNewHero = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Swingly");
        setName("newFrame"); // NOI18N
        setPreferredSize(new java.awt.Dimension(500, 500));
        setResizable(false);

        pnlMain.setBackground(new java.awt.Color(51, 51, 51));

        lblHeros.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/Heros.png")); // NOI18N

        pnlButtons.setBackground(new java.awt.Color(51, 51, 51));

        tfName.setFont(new java.awt.Font("Courier New", 1, 36)); // NOI18N
        tfName.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfName.setText("NAME");

        btnContinue.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonRight.png")); // NOI18N
        btnContinue.setToolTipText("Continue");
        btnContinue.setActionCommand("ContinueNewHero");
        btnContinue.setContentAreaFilled(false);
        btnContinue.setMaximumSize(new java.awt.Dimension(75, 75));
        btnContinue.setMinimumSize(new java.awt.Dimension(75, 75));
        btnContinue.setPreferredSize(new java.awt.Dimension(75, 75));
        btnContinue.setPressedIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonRightSelected.png")); // NOI18N

        btnBack.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonLeft.png")); // NOI18N
        btnBack.setToolTipText("Go back");
        btnBack.setActionCommand("Back");
        btnBack.setContentAreaFilled(false);
        btnBack.setMaximumSize(new java.awt.Dimension(75, 75));
        btnBack.setMinimumSize(new java.awt.Dimension(75, 75));
        btnBack.setPreferredSize(new java.awt.Dimension(75, 75));
        btnBack.setPressedIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonLeftSelected.png")); // NOI18N

        javax.swing.GroupLayout pnlButtonsLayout = new javax.swing.GroupLayout(pnlButtons);
        pnlButtons.setLayout(pnlButtonsLayout);
        pnlButtonsLayout.setHorizontalGroup(
            pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonsLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnContinue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        pnlButtonsLayout.setVerticalGroup(
            pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonsLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonsLayout.createSequentialGroup()
                        .addGroup(pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnContinue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBack, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())))
        );

        lblNewHero.setBackground(new java.awt.Color(102, 102, 102));
        lblNewHero.setFont(new java.awt.Font("Comic Sans MS", 1, 36)); // NOI18N
        lblNewHero.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNewHero.setText("NAME THE CLASS");
        lblNewHero.setOpaque(true);

        javax.swing.GroupLayout pnlMainLayout = new javax.swing.GroupLayout(pnlMain);
        pnlMain.setLayout(pnlMainLayout);
        pnlMainLayout.setHorizontalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainLayout.createSequentialGroup()
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlMainLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(sbHeroClass, javax.swing.GroupLayout.PREFERRED_SIZE, 479, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlMainLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblNewHero, javax.swing.GroupLayout.PREFERRED_SIZE, 454, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pnlButtons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlMainLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(lblHeros, javax.swing.GroupLayout.PREFERRED_SIZE, 447, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(35, 35, 35))
        );
        pnlMainLayout.setVerticalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblHeros, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sbHeroClass, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlButtons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblNewHero)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.PREFERRED_SIZE, 496, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }

    private JButton btnBack;
    private JButton btnContinue;
    private JLabel lblHeros;
    private JLabel lblNewHero;
    private JPanel pnlButtons;
    private JPanel pnlMain;
    private JSlider sbHeroClass;
    private JTextField tfName;
}
