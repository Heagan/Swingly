package src.swingly.view;

import lombok.Data;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

import static src.swingly.Swing.displayModel;

@Data
public class GameDisplayView extends JFrame {
    public GameDisplayView() {
        initComponents();

        tfDetails.setEditable(false);
    }

    public void addKeyListeners(KeyListener buttonPressedListener) {
        btnDown.addKeyListener(buttonPressedListener);
        btnUp.addKeyListener(buttonPressedListener);
        btnLeft.addKeyListener(buttonPressedListener);
        btnRight.addKeyListener(buttonPressedListener);
    }

    public void addButtonListeners(ActionListener buttonClickListener) {
        btnDown.addActionListener(buttonClickListener);
        btnUp.addActionListener(buttonClickListener);
        btnLeft.addActionListener(buttonClickListener);
        btnRight.addActionListener(buttonClickListener);
    }

    @Override
    public void setVisible(boolean b) {
        btnUp.requestFocus();
        super.setVisible(b);
    }

    public void wonGame() {
        displayModel.displayMessage("YOU HAVE WON THE GAME!!!!");
    }

    private void initComponents() {

        pnlMain = new javax.swing.JPanel();
        tfDetails = new javax.swing.JTextArea();
        pnlMovement = new javax.swing.JPanel();
        btnUp = new javax.swing.JButton();
        btnDown = new javax.swing.JButton();
        btnRight = new javax.swing.JButton();
        btnLeft = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Swingly");
        setName("gameFrame"); // NOI18N
        setPreferredSize(new java.awt.Dimension(170, 410));
        setResizable(false);
        setSize(new java.awt.Dimension(170, 410));

        pnlMain.setBackground(new java.awt.Color(51, 51, 51));
        pnlMain.setPreferredSize(new java.awt.Dimension(200, 380));

        tfDetails.setEditable(true);
        //tfDetails.setColumns(20);
        //tfDetails.setRows(5);
        tfDetails.setText("Name:\nClass:\nLevel:\nExperence:\n\nHealth:\nArmor:\nWeapon:\nHelm:");
        tfDetails.setMaximumSize(new java.awt.Dimension(230, 155));
        tfDetails.setMinimumSize(new java.awt.Dimension(230, 155));
        tfDetails.setPreferredSize(new java.awt.Dimension(230, 155));

        pnlMovement.setForeground(new java.awt.Color(102, 102, 102));
        pnlMovement.setPreferredSize(new java.awt.Dimension(175, 175));

        btnUp.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonUp.png")); // NOI18N
        btnUp.setActionCommand("Up");
        btnUp.setContentAreaFilled(false);
        btnUp.setPressedIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonUpSelected.png")); // NOI18N

        btnDown.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonDown.png")); // NOI18N
        btnDown.setActionCommand("Down");
        btnDown.setContentAreaFilled(false);
        btnDown.setPressedIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonDownSelected.png")); // NOI18N

        btnRight.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonRight.png")); // NOI18N
        btnRight.setActionCommand("Right");
        btnRight.setContentAreaFilled(false);
        btnRight.setPressedIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonRightSelected.png")); // NOI18N

        btnLeft.setIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonLeft.png")); // NOI18N
        btnLeft.setActionCommand("Left");
        btnLeft.setContentAreaFilled(false);
        btnLeft.setPressedIcon(new javax.swing.ImageIcon("src/main/java/src/swingly/images/buttons/small/buttonLeftSelected.png")); // NOI18N

        javax.swing.GroupLayout pnlMovementLayout = new javax.swing.GroupLayout(pnlMovement);
        pnlMovement.setLayout(pnlMovementLayout);
        pnlMovementLayout.setHorizontalGroup(
                pnlMovementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMovementLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnLeft, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(pnlMovementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(btnUp, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnDown, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRight, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );
        pnlMovementLayout.setVerticalGroup(
                pnlMovementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlMovementLayout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(pnlMovementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(pnlMovementLayout.createSequentialGroup()
                                                .addComponent(btnUp, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                                                .addComponent(btnDown, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(pnlMovementLayout.createSequentialGroup()
                                                .addGroup(pnlMovementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(pnlMovementLayout.createSequentialGroup()
                                                                .addGap(45, 45, 45)
                                                                .addComponent(btnLeft, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(pnlMovementLayout.createSequentialGroup()
                                                                .addGap(48, 48, 48)
                                                                .addComponent(btnRight, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
        );

        javax.swing.GroupLayout pnlMainLayout = new javax.swing.GroupLayout(pnlMain);
        pnlMain.setLayout(pnlMainLayout);
        pnlMainLayout.setHorizontalGroup(
                pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlMainLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(tfDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(pnlMovement, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlMainLayout.setVerticalGroup(
                pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlMainLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(tfDetails, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pnlMovement, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(16, 16, 16))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(pnlMain, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(pnlMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }

    private JButton btnDown;
    private JButton btnLeft;
    private JButton btnRight;
    private JButton btnUp;
    private JPanel pnlMain;
    private JPanel pnlMovement;
    private JTextArea tfDetails;
}
