package src.swingly.view;

import javax.swing.*;

public class BattleDisplayPanelView extends JPanel {

    public BattleDisplayPanelView() {
        initComponents();
    }

    public void updateInformation(String actorName, String actorStats, int pbValue) {
        pbHealth.setValue(pbValue);
        lblAttributes.setText(actorStats);
        lblName.setText(actorName);
    }

    private void initComponents() {

        lblName = new javax.swing.JLabel();
        lblAttributes = new javax.swing.JLabel();
        pbHealth = new javax.swing.JProgressBar();

        setBackground(new java.awt.Color(102, 102, 102));

        lblName.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        lblName.setForeground(new java.awt.Color(255, 255, 255));
        lblName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblName.setText("Lvl X Actor X");

        lblAttributes.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        lblAttributes.setForeground(new java.awt.Color(255, 255, 255));
        lblAttributes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAttributes.setText("X/X A:X D:X");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(pbHealth, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblName)
                            .addComponent(lblAttributes))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblAttributes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pbHealth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }

    private JLabel lblAttributes;
    private JLabel lblName;
    private JProgressBar pbHealth;
}
