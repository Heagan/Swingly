package src.swingly.view;

import lombok.Data;

@Data
public class MapDisplayView extends javax.swing.JFrame {

    public MapDisplayView() {
        initComponents();
    }

    public void setText(String text) {
        tfMap.setText(text);
    }

    public void appendText(String text) {
        tfMap.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        tfMap.append(text);
    }

    private void initComponents() {


        pnlMap = new javax.swing.JPanel();
        tfMap = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Swingly");
        setModalExclusionType(null);
        setName("mapFrame"); // NOI18N
        setPreferredSize(new java.awt.Dimension(500, 500));
        setResizable(false);
        setType(java.awt.Window.Type.POPUP);

        pnlMap.setBackground(new java.awt.Color(51, 51, 51));
        pnlMap.setPreferredSize(new java.awt.Dimension(500, 500));

        tfMap.setEditable(false);
        tfMap.setBackground(new java.awt.Color(255, 255, 255));
        tfMap.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        tfMap.setText("* * * * * * * * * *\n* * * * * * * * * *\n* * * E E E E * * *\n* * * * * * * * * *\n* * * * P * * * * *\n* * * * * * * * * *\n* * * E E E E * * *\n* * * * * * * * * *\n* * * * * * * * * *\n* * * * * * * * * *");
        tfMap.setToolTipText("");
        tfMap.setMinimumSize(new java.awt.Dimension(0, 0));
        tfMap.setPreferredSize(new java.awt.Dimension(500, 500));

        javax.swing.GroupLayout pnlMapLayout = new javax.swing.GroupLayout(pnlMap);
        pnlMap.setLayout(pnlMapLayout);
        pnlMapLayout.setHorizontalGroup(
                pnlMapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlMapLayout.createSequentialGroup()
                                .addComponent(tfMap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );
        pnlMapLayout.setVerticalGroup(
                pnlMapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlMapLayout.createSequentialGroup()
                                .addComponent(tfMap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlMap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlMap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }

    private javax.swing.JButton btnZoomIn;
    private javax.swing.JButton btnZoomOut;
    private javax.swing.JPanel pnlMap;
    private javax.swing.JPanel pnlZoom;
    private javax.swing.JTextArea tfMap;
}
