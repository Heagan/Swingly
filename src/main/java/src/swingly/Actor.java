package src.swingly;

import lombok.Data;
import src.swingly.dependencies.Location;
import src.swingly.dependencies.RandomFactor;

import static src.swingly.Swing.actorModel;
import static src.swingly.Swing.displayModel;

@Data
public class Actor {

    private String actorName;
    private String actorClass;
    private int actorLevel;
    private double actorExperience;
    private double requiredExpForNextLevel;
    private double actorDefence;
    private double actorAttack;
    private double actorHealth;
    private RandomFactor randomFactor;
    private Equipment equipment;
    private Location location;
    private boolean isBoss;

    public Actor(String name, String actorClass, int actorLevel, double actorExperience, Equipment equipment) {
        this.actorName = name;
        this.actorClass = actorClass;
        this.actorLevel = actorLevel;
        this.actorExperience = actorExperience;
        this.randomFactor = new RandomFactor();
        this.equipment = equipment;
        this.location = new Location(0, 0);
        this.isBoss = false;
        setRequiredExpForNextLevel();
    }

    public void setActorLevel(int level) {
        this.actorLevel = level;
        actorModel.refreshActorsStats(this);
    }

    private void setRequiredExpForNextLevel() {
        int n;

        n = actorLevel + 1;
        requiredExpForNextLevel = (n * 1000 + ((n - 1) * (n - 1)) * 450) - 1500;
    }

    public void appendExperience(double exp) {
        setActorExperience(getActorExperience() + exp);

        if (requiredExpForNextLevel == 0) {
            setRequiredExpForNextLevel();
        }
        while (actorExperience > requiredExpForNextLevel) {
            actorLevel++;
            displayModel.displayMessage( "LEVEL UP! You are now Lv:" + actorLevel);
            setRequiredExpForNextLevel();
        }
    }

    public void appendLocation(int x, int y) {
        x += this.location.getX();
        y += this.location.getY();

        //space not occupied or its the hero
        //hero can go anywhere
        if (actorModel.isLocationUnique(x, y) || isGoodOrEvil()) {
            //is it in the map?
            if (!((x >= Swing.mapModel.getMapSize() || x < 0) || (y >= Swing.mapModel.getMapSize() || y < 0)))
            {
                this.location.setX(x);
                this.location.setY(y);
            }
        }
    }

    public void setLocation(int x, int y) {
        this.location.setX(x);
        this.location.setY(y);
    }

    public boolean isGoodOrEvil() {
        if (actorClass.contentEquals("Warrior") || actorClass.contentEquals("Archer") || actorClass.contentEquals("Fighter")) {
            return true;
        }
        return false;
    }

    public void moveRandom() {
        int direction = (int)Math.round(Math.random() * 10);

        switch (direction) {
            case(0): {
                this.appendLocation(1, 0);
                break ;
            }
            case(1): {
                this.appendLocation(-1, 0);
                break ;
            }
            case(2): {
                this.appendLocation(0, 1);
                break ;
            }
            case(3): {
                this.appendLocation(0, -1);
                break ;
            }
            default: {
                this.appendLocation(0, 0);
                break ;
            }
        }
    }
}
