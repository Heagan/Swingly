package src.swingly.dependencies;

import lombok.Data;

@Data
public class Location {
    private int x;
    private int y;

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Location(Location location) {
        this.x = location.getX();
        this.y = location.getY();
    }

    public void appendLocation(int x, int y) {
        x += this.x;
        y += this.y;

        //is it in the map?
        this.x = x;
        this.y = y;
    }
}
