package src.swingly.dependencies;

import lombok.Data;
import src.swingly.filemanagment.Read;

import java.util.ArrayList;

@Data
public class Names {
    private static ArrayList<String> names = new ArrayList<String>();

    public static ArrayList<String>  loadNames(String file) {
        String readLine;

        Read.openFile(file);
        while ((readLine = Read.readLn()) != null) {
            names.add(readLine);
        }
        Read.closeFile();
        return names;
    }
}
