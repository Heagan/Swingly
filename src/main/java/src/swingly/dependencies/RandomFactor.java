package src.swingly.dependencies;

import lombok.Data;

public @Data class RandomFactor {
    private double  dropChance;
    private double  critChance;
    private double  fleeChance;
    private double  handiCap;

    public RandomFactor() {
        this.dropChance =   Math.random();
        this.critChance =   Math.random();
        this.handiCap   =   Math.random();
    }

    public double getCritChance() {
        critChance = Math.random();
        return (critChance);
    }

    public double getFleeChance() {
        fleeChance = Math.random();
        return (fleeChance);
    }

    public double getDropChance() {
        dropChance = Math.random();
        return (dropChance);
    }

    public double getHandicap() {
        handiCap = Math.random();
        return (handiCap);
    }
}
