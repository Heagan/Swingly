package src.swingly.filemanagment;

import src.swingly.Swing;

import java.io.*;

public class Read {
	public static String line = null;
	public static BufferedReader bufferedReader;
    static String fileName;
    static FileReader fileReader;

	public static void     openFile( String file ){
		File pfile = new File(file);
		if (!pfile.exists() || pfile.isDirectory())
			Swing.Error("File " + file + " is invalid!");
		try {
			fileName = file;
            fileReader = new FileReader(fileName);
            bufferedReader = new BufferedReader(fileReader);
		}
		catch(FileNotFoundException ex) {
			Swing.Error("Unable to open file '" + fileName + "'");
		}
	}

	public static void     openFile( String file, Boolean create ){
		File pfile = new File(file);

		if (!pfile.exists() || pfile.isDirectory()) {
			if (create) {
				Write.openFile(file);
				Write.closeFile();
			}
		}
		try {
			fileName = file;
			fileReader = new FileReader(fileName);
			bufferedReader = new BufferedReader(fileReader);
		}
		catch(FileNotFoundException ex) {
			Swing.Error("Unable to open file '" + fileName + "'");
		}
	}

	public static String readLn() {
		try {
			while ((line = bufferedReader.readLine()) != null) {
				return (line);
			}
		}
		catch (IOException ex) {
			Swing.Error("Error reading file '" + fileName + "'");
		}
		return (null);
	}

	public static void closeFile() {
        try {
            bufferedReader.close();
        }
        catch (IOException ex) {
			Swing.Error("Error closing file '" + fileName + "'");
        }
    }

    protected void finalize() {
		try {
			bufferedReader.close();
		}
		catch (IOException ex) {
			Swing.Error("Error closing file '" + fileName + "'");
		}
	}
}
