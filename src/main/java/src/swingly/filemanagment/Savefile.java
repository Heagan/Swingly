package src.swingly.filemanagment;

import src.swingly.Actor;

import java.util.ArrayList;

import static src.swingly.Swing.mapModel;

public class Savefile {
    public static boolean loadSaveData(ArrayList<String[]> data ) {
        String line;
        String lineElem[];

        Read.openFile("savefile.sav", true);
        while ((line = Read.readLn()) != null) {
            lineElem = line.split(",");
            if (lineElem.length != 7) {
                System.out.println("Bad Savefile Existing");
                return false;
            }
            if (!(lineElem[1].contains("Warrior")) && !(lineElem[1].contains("Archer")) && !(lineElem[1].contains("Fighter"))) {
                System.out.println("Bad Savefile Existing");
                return false;
            }
            data.add(lineElem);
        }
        Read.closeFile();
        return true;
    }

    public static void saveGame () {
        Actor   hero;
        //info you want to save
        String  saveInfo;
        //save files current info
        String  saveFileCurrentInfo;
        String  readLine;
        String  equipmentInfo;

        hero = mapModel.getHero();
        saveInfo = hero.getActorName() + ",";
        saveInfo += hero.getActorClass() + ",";
        saveInfo += hero.getActorLevel() + ",";
        saveInfo += hero.getActorExperience() + ",";

        //.replaceAll("\\s+","") get rid of spaces
        //there's a weird behaviour where an extra
        //space is added every time you save
        equipmentInfo = hero.getEquipment().getWeaponDetails();
        saveInfo += equipmentInfo.replaceAll("\\s+","") + ",";
        equipmentInfo = hero.getEquipment().getArmorDetails();
        saveInfo += equipmentInfo.replaceAll("\\s+","") + ",";
        equipmentInfo = hero.getEquipment().getHelmDetails();
        saveInfo += equipmentInfo.replaceAll("\\s+","") + ",";

        saveFileCurrentInfo = "";
        Read.openFile("savefile.sav", true);
        while ((readLine = Read.readLn()) != null) {
            if (!saveInfo.contains(readLine.split(",")[0])) {
                saveFileCurrentInfo += readLine + "\n";
            }
        }
        Read.closeFile();

        Write.openFile("savefile.sav");
        Write.write(saveFileCurrentInfo);
        Write.writeLn(saveInfo);
        Write.closeFile();
    }
}
