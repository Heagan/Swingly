package src.swingly.filemanagment;

import src.swingly.Swing;

import java.io.*;

public class Write {
    private static FileWriter fileWriter;
    private static BufferedWriter bufferedWriter;

    public static void openFile( String filename ) {

        try {
            fileWriter = new FileWriter(filename);
            bufferedWriter = new BufferedWriter(fileWriter);
        }
        catch(IOException ex) {
            Swing.Error("Unable to open file '");
        }
    }

    public static int writeLn(String line) {
        try {
            bufferedWriter.write(line);
            bufferedWriter.newLine();
        } catch (IOException ex) {
            Swing.Error("Error writing to file!");
        }
        return (1);
    }

    public static int write(String line) {
        try {
            bufferedWriter.write(line);
        } catch (IOException ex) {
            Swing.Error("Error writing to file!");
        }
        return (1);
    }

    public static void closeFile() {
        try {
            bufferedWriter.close();
        } catch (IOException ex) {
            Swing.Error("Error closing file!");
        }
    }

    protected void finalize() {
        try {
            bufferedWriter.close();
        } catch (IOException ex) {
            Swing.Error("Error closing file!");
        }
    }
}
