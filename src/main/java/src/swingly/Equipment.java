package src.swingly;

import lombok.Data;

@Data
public class Equipment {
    private Weapon weapon;
    private Armor armor;
    private Helm helm;

    public Equipment() {
        weapon = new Weapon("Hands", "Hero");
        armor = new Armor("Chest", "Hero");
        helm = new Helm("Hair", "Hero");
    }

    public Equipment(Equipment equipment) {
        weapon = new Weapon(equipment.getWeaponDetails().split(" ")[0], equipment.getWeaponDetails().split(" ")[1]);
        armor = new Armor(equipment.getArmorDetails().split(" ")[0], equipment.getArmorDetails().split(" ")[1]);
        helm = new Helm(equipment.getHelmDetails().split(" ")[0], equipment.getHelmDetails().split(" ")[1]);
    }

    public String getWeaponDetails() {
        return (this.weapon.getWeaponName() + " " + this.weapon.getWeaponClass());
    }
    public String getArmorDetails() {
        return (this.armor.getArmorName() + " " + this.armor.getArmorClass());
    }
    public String getHelmDetails() {
        return (this.helm.getHelmName() + " " + this.helm.getHelmClass());
    }

    public String getQuality( String quality ) {
        if (quality.contains("Hero")) {
            return ("Hero");
        }
        if (quality.contains("Wooden")) {
            return ("Wooden");
        }
        if (quality.contains("Iron")) {
            return ("Iron");
        }
        if (quality.contains("Diamond")) {
            return ("Diamond");
        }
        return ("Hero");
    }
    public String getNextBestQuality( String quality ) {
        if (quality.contains("Hero")) {
            return ("Wooden");
        }
        if (quality.contains("Wooden")) {
            return ("Iron");
        }
        if (quality.contains("Iron")) {
            return ("Diamond");
        }
        if (quality.contains("Diamond")) {
            return ("Diamond");
        }
        return ("Hero");
    }

    public void addWeapon(String weaponDetails) {
        String name;
        String quality;

        quality = "Hero";
        if (weaponDetails.contains("Wooden")) {
            quality = "Wooden";
        }
        if (weaponDetails.contains("Iron")) {
            quality = "Iron";
        }
        if (weaponDetails.contains("Diamond")) {
            quality = "Diamond";
        }

        name = weaponDetails.split(quality)[0];
        Weapon weapon = new Weapon(name, quality);
        this.setWeapon(weapon);
    }
    public void addArmor(String armorDetails) {
        String name;
        String quality;

        quality = "Hero";
        if (armorDetails.contains("Wooden")) {
            quality = "Wooden";
        }
        if (armorDetails.contains("Iron")) {
            quality = "Iron";
        }
        if (armorDetails.contains("Diamond")) {
            quality = "Diamond";
        }

        name = armorDetails.split(quality)[0];
        Armor armor = new Armor(name, quality);
        this.setArmor(armor);
    }
    public void addHelm(String helmDetails) {
        String name;
        String quality;

        quality = "Hero";
        if (helmDetails.contains("Wooden")) {
            quality = "Wooden";
        }
        if (helmDetails.contains("Iron")) {
            quality = "Iron";
        }
        if (helmDetails.contains("Diamond")) {
            quality = "Diamond";
        }

        name = helmDetails.split(quality)[0];
        Helm helm = new Helm(name, quality);
        this.setHelm(helm);
    }
}

@Data
class Weapon {
    private String weaponName;
    private String weaponClass;

    Weapon(String name, String quality) {
        weaponName = name;
        weaponClass = quality;
    }
}

@Data
class Armor {
    private String ArmorName;
    private String ArmorClass;

    Armor(String name, String quality) {
        ArmorName = name;
        ArmorClass = quality;
    }
}

@Data
class Helm {
    private String HelmName;
    private String HelmClass;

    Helm(String name, String quality) {
        HelmName = name;
        HelmClass = quality;
    }
}